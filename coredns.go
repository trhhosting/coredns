package main

//go:generate go run directives_generate.go
//go:generate go run owners_generate.go

import (
	_ "gitlab.com/trhhosting/coredns/core/plugin" // Plug in CoreDNS.
	"gitlab.com/trhhosting/coredns/coremain"
)

func main() {
	coremain.Run()
}
