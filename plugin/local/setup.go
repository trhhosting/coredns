package local

import (
	"gitlab.com/trhhosting/caddy"
	"gitlab.com/trhhosting/coredns/core/dnsserver"
	"gitlab.com/trhhosting/coredns/plugin"
)

func init() { plugin.Register("local", setup) }

func setup(c *caddy.Controller) error {
	l := Local{}

	dnsserver.GetConfig(c).AddPlugin(func(next plugin.Handler) plugin.Handler {
		l.Next = next
		return l
	})

	return nil
}
