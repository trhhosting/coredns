package any

import (
	"gitlab.com/trhhosting/caddy"
	"gitlab.com/trhhosting/coredns/core/dnsserver"
	"gitlab.com/trhhosting/coredns/plugin"
)

func init() { plugin.Register("any", setup) }

func setup(c *caddy.Controller) error {
	a := Any{}

	dnsserver.GetConfig(c).AddPlugin(func(next plugin.Handler) plugin.Handler {
		a.Next = next
		return a
	})

	return nil
}
