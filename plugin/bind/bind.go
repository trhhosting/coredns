// Package bind allows binding to a specific interface instead of bind to all of them.
package bind

import "gitlab.com/trhhosting/coredns/plugin"

func init() { plugin.Register("bind", setup) }
