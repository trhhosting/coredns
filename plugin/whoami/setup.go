package whoami

import (
	"gitlab.com/trhhosting/caddy"
	"gitlab.com/trhhosting/coredns/core/dnsserver"
	"gitlab.com/trhhosting/coredns/plugin"
)

func init() { plugin.Register("whoami", setup) }

func setup(c *caddy.Controller) error {
	c.Next() // 'whoami'
	if c.NextArg() {
		return plugin.Error("whoami", c.ArgErr())
	}

	dnsserver.GetConfig(c).AddPlugin(func(next plugin.Handler) plugin.Handler {
		return Whoami{}
	})

	return nil
}
