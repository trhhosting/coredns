// +build gofuzz

package cache

import (
	"gitlab.com/trhhosting/coredns/plugin/pkg/fuzz"
)

// Fuzz fuzzes cache.
func Fuzz(data []byte) int {
	return fuzz.Do(New(), data)
}
