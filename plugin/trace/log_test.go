package trace

import clog "gitlab.com/trhhosting/coredns/plugin/pkg/log"

func init() { clog.Discard() }
